# VDay reminder

To easily deploy on a server, run the Gradle shadowJar task, the self-contained jar will be in `build/libs`.
Then just run it with `java -jar vdien-reminder.jar`.

This is generally meant to be run as a cron job once a day.