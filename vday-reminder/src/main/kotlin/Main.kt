import at.bitfire.dav4jvm.BasicDigestAuthHandler
import at.bitfire.dav4jvm.DavAddressBook
import at.bitfire.dav4jvm.property.AddressData
import ezvcard.VCard
import ezvcard.io.text.VCardReader
import ezvcard.property.StructuredName
import okhttp3.HttpUrl
import okhttp3.HttpUrl.Companion.toHttpUrl
import okhttp3.OkHttpClient
import org.apache.commons.lang3.exception.ExceptionUtils
import org.jsoup.Jsoup
import kotlin.system.exitProcess

fun getEnv(key: String): String {
    return System.getenv(key) ?: throw Exception("$key not specified")
}


fun main() {
    val envPrefix = "VDIEN"
    try {
        val davUser = getEnv("${envPrefix}_DAV_USER")
        val davPassword = getEnv("${envPrefix}_DAV_PASSWORD")
        val cardDavUrl = getEnv("${envPrefix}_DAV_URL")

        val authHandler = BasicDigestAuthHandler(
            domain = null, username = davUser, password = davPassword
        )
        val davHttpClient =
            OkHttpClient.Builder().followRedirects(false).authenticator(authHandler).addNetworkInterceptor(authHandler)
                .build()

        val cards = getContactCards(davHttpClient, cardDavUrl.toHttpUrl())

        val doc = Jsoup.connect("https://inbox.lv/").get()
        val vdayText = doc.getElementsByClass("nav-namedays").first()?.text()
            ?: throw Exception("Inbox lapā nav atrodamas vārda dienas")
        val vdays = splitFirstNameList(vdayText)

        val vdayCards = cards.filter { card -> vdays.contains(card.structuredName.getCleanFirstName()) }
        val msg = if (vdayCards.isNotEmpty()) {
            val names = vdayCards.joinToString("\n") { card -> card.formattedName.value }
            "Šodien vārda dienu svin:\n$names"
        } else {
            "Šodien neviens kontakts nesvin vārda dienu :("
        }

        sendNotificationMsg(msg)
    } catch (ex: Throwable) {
        sendNotificationMsg(
            "Vārda dienu meklētājs nomira: ${ex.message}\n${ExceptionUtils.getStackTrace(ex)}"
        )
        exitProcess(1)
    }
}

fun getContactCards(httpClient: OkHttpClient, location: HttpUrl): List<VCard> {
    val cards = ArrayList<VCard>()
    val cal = DavAddressBook(httpClient, location)
    cal.propfind(1, AddressData.NAME) { response, _ ->
        for (propstat in response.propstat) {
            for (prop in propstat.properties) {
                if (prop is AddressData && prop.card != null) {
                    val reader = VCardReader(prop.card)
                    val card = reader.readNext()
                    cards.add(card)
                }
            }
        }
    }
    return cards
}

fun StructuredName.getCleanFirstName(): String {
    return this.given.splitToSequence(' ').first().trim()
}

fun splitFirstNameList(vdayText: String): List<String> {
    val withoutWhitespace = vdayText.filterNot { it.isWhitespace() }
    return withoutWhitespace.split(',')
}

fun sendNotificationMsg(msg: String) {
    print(msg)
}