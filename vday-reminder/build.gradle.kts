plugins {
    kotlin("jvm") version "2.1.0"
    // For finding dep updates
    id("com.github.ben-manes.versions") version "0.51.0"
    // For building self-contained jars
    id("com.github.johnrengelman.shadow") version "8.1.1"
    application
}

group = "lv.id.kilis"
version = "1.2"

repositories {
    mavenCentral()
    // for dav4jvm
    maven { url = uri("https://jitpack.io") }
}

dependencies {
    // DAV client
    implementation("com.github.bitfireAT:dav4jvm:2.2.1")
    // VCard parser
    implementation("com.googlecode.ez-vcard:ez-vcard:0.12.1")
    // HTML parser
    implementation("org.jsoup:jsoup:1.18.3")
    // General utilities
    implementation("org.apache.commons:commons-lang3:3.17.0")
}

kotlin {
    jvmToolchain(17)
}

application {
    mainClass.set("MainKt")
}

// specify main class for Jar file
tasks.withType<Jar> {
    manifest {
        attributes["Main-Class"] = "MainKt"
    }
}
