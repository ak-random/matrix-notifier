use serde::{Deserialize, Serialize};

fn riga_tz() -> tzfile::ArcTz {
    use std::sync::OnceLock;
    static INSTANCE: OnceLock<tzfile::ArcTz> = OnceLock::new();
    INSTANCE
        .get_or_init(|| tzfile::ArcTz::named("Europe/Riga").unwrap())
        .clone()
}

#[derive(Serialize)]
struct LoginRequest {
    email: String,
    password: String,
}

#[derive(Deserialize)]
struct LoginResponse {
    token: String,
}

#[derive(Debug, Deserialize)]
struct AvailableMenusResponse {
    dishlists: Vec<MenuMeta>,
}

fn riga_date_time<'de, D>(input: D) -> Result<chrono::DateTime<tzfile::ArcTz>, D::Error>
where
    D: serde::Deserializer<'de>,
{
    let date_string = String::deserialize(input)?;
    let naive = chrono::NaiveDateTime::parse_from_str(&date_string, "%Y-%m-%d %H:%M:%S")
        .map_err(serde::de::Error::custom)?;
    let riga_time =
        naive
            .and_local_timezone(riga_tz())
            .single()
            .ok_or(serde::de::Error::custom(
                "Time could not be definitively converted to Riga time",
            ))?;
    Ok(riga_time)
}

#[derive(Debug, Deserialize)]
struct MenuMeta {
    id: String,
    #[serde(deserialize_with = "riga_date_time")]
    from: chrono::DateTime<tzfile::ArcTz>,
    #[serde(deserialize_with = "riga_date_time")]
    to: chrono::DateTime<tzfile::ArcTz>,
}

#[derive(Debug, Deserialize)]
struct Menu {
    dishlist: Dishlist,
}

#[derive(Debug, Deserialize)]
struct Dishlist {
    categories: Vec<Category>,
}

#[derive(Debug, Deserialize)]
struct Category {
    #[allow(unused)]
    title: MultiLingualTitle,
    dishes: Vec<Dish>,
}

#[derive(Debug, Deserialize)]
struct MultiLingualTitle {
    #[allow(unused)]
    en: String,
    lv: String,
    #[allow(unused)]
    ru: String,
}

#[derive(Debug, Deserialize)]
struct Dish {
    title: MultiLingualTitle,
}

const BASE_API_URL: &'static str = "https://api.lunch2.work";
const ENV_PREFIX: &'static str = "FOOD";

fn read_env_var(name: &str) -> color_eyre::Result<String> {
    Ok(std::env::var(format!("{ENV_PREFIX}_{name}"))?)
}

fn authenticate() -> color_eyre::Result<String> {
    let client = reqwest::blocking::Client::new();
    let token = client
        .post(format!("{BASE_API_URL}/app/login"))
        .header("Origin", "https://app.foodclub.lv")
        .json(&LoginRequest {
            email: read_env_var("EMAIL")?,
            password: read_env_var("PASSWORD")?,
        })
        .send()?
        .json::<LoginResponse>()?
        .token;
    Ok(format!("Bearer {token}"))
}

fn get_authd_client(token: &str) -> color_eyre::Result<reqwest::blocking::Client> {
    use reqwest::header::{HeaderMap, HeaderValue};
    let mut default_headers = HeaderMap::new();
    default_headers.insert("Authorization", HeaderValue::from_str(&token)?);
    let client = reqwest::blocking::Client::builder()
        .default_headers(default_headers)
        .build()?;
    Ok(client)
}

fn main() -> color_eyre::Result<()> {
    if std::env::var("NO_COLOR") == Err(std::env::VarError::NotPresent) {
        color_eyre::install()?;
    } else {
        color_eyre::config::HookBuilder::new()
            .theme(color_eyre::config::Theme::new())
            .install()?;
    }

    let token = authenticate()?;
    let client = get_authd_client(&token)?;
    let dishlists = client
        .get(format!("{BASE_API_URL}/app/order/dishlists"))
        .query(&[("lang", "lv")])
        .send()?
        .json::<AvailableMenusResponse>()?;

    let now = chrono::Local::now();
    let menu_metas = dishlists
        .dishlists
        .into_iter()
        .filter(|x| x.from <= now && now <= x.to);

    let mut menus = Vec::new();
    for menu_meta in menu_metas {
        let menu_id = menu_meta.id;
        let menu = client
            .get(format!("{BASE_API_URL}/app/order/create/{menu_id}"))
            .query(&[("lang", "lv")])
            .send()?
            .json::<Menu>()?;
        menus.push(menu);
    }

    let titles: Vec<_> = menus
        .into_iter()
        .flat_map(|menu| {
            menu.dishlist
                .categories
                .into_iter()
                .flat_map(|cat| cat.dishes.into_iter().map(|dish| dish.title.lv))
        })
        .filter(|t| {
            let lower = t.to_lowercase();
            lower.contains("lazanja")
        })
        .collect();

    if titles.len() > 0 {
        println!("Foodclub varētu būt kaut kas interesants! Kandidāti:");
        for title in titles {
            println!("{title}");
        }
    }

    Ok(())
}

#[cfg(test)]
mod tests {
    use crate::AvailableMenusResponse;
    use chrono::TimeZone;

    #[test]
    fn riga_date_time_test() {
        let input = r#"{"dishlists":[{"id":"1895","title":"11.12.2023.","from":"2023-12-10 10:30:00","to":"2023-12-11 09:30:00","status":true,"provider_id":1}]}"#;
        let item: AvailableMenusResponse = serde_json::from_str(input).unwrap();
        let first = item.dishlists.first().unwrap();

        let riga = crate::riga_tz();
        assert_eq!(
            first.from,
            riga.with_ymd_and_hms(2023, 12, 10, 10, 30, 00).unwrap()
        );
        assert_eq!(
            first.to,
            riga.with_ymd_and_hms(2023, 12, 11, 9, 30, 00).unwrap()
        );
    }
}
