Build and publish the matrix notifier:
```sh
docker login registry.gitlab.com
docker build -f notifier.Dockerfile -t registry.gitlab.com/ak-random/matrix-notifier .
docker push registry.gitlab.com/ak-random/matrix-notifier
```
